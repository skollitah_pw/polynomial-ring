import unittest

import main


class TestEuclidian(unittest.TestCase):

    def test_sum(self):
        self.assertEqual(main.sum(0x57, 0x02), 0b01010101)
        self.assertEqual(main.sum(0x53, 0xCA), 153)
        self.assertEqual(main.sum(0x03, 0x03), 0b00000000)
        self.assertEqual(main.sum(0xFF, 0x0F), 0b11110000)
        self.assertEqual(main.sum(0b01010111, 0b10000011), 0b11010100)

    def test_multiply(self):
        # self.assertEqual(main.multiply_poly(0x57, 0x02), 0b00000010)
        # self.assertEqual(main.multiply(0x57, 0x04), 0b00000100)
        # self.assertEqual(main.multiply(0x57, 0x10), 0b00010000)
        # self.assertEqual(main.multiply(0x57, 0x83), 0xC1)
        print(format((0x57 * 0x02), '016b'))
        print(format((0x57 * 0x02) % 0x11b, '016b'))
        print(format((0x57 * 0x04), '016b'))
        print(format((0x57 * 0x04) % 0x11b, '016b'))
        print(format((0x57 * 0x10), '016b'))
        print(format((0x57 * 0x10) % 0x11b, '016b'))
        print(format((0b01010111 * 0b10000011), '016b'))
        print(format((0b01010111 * 0b10000011) % 0x11b, '016b'))

    def test_convert_num_to_bit_array(self):
        print(format(( 0b11111101111110  % 0b100011011), '016b'))


if __name__ == '__main__':
    unittest.main()
