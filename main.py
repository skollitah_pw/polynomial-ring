def sum(a, b):
    return a ^ b


def printPoly(poly):
    n = len(poly)

    for i in range(n):
        if (poly[i] != 0):
            if (i == 0):
                print(poly[i], end="");
            if (i > 1):
                print(f"x^{i}", end="");
            if (i == 1):
                print("x", end="");
            if (i != n - 1):
                print(" + ", end="");


# https://www.geeksforgeeks.org/multiply-two-polynomials-2/
def multiply_poly(a, b):
    A = [int(x) for x in list('{0:0b}'.format(a))]
    B = [int(x) for x in list('{0:0b}'.format(b))]

    A.reverse()
    B.reverse()

    m = len(A);
    n = len(B);
    prod = [0] * (m + n - 1);

    for i in range(m):
        for j in range(n):
            prod[i + j] += A[i] * B[j];

    for i in range(m + n - 1):
        prod[i] = prod[i] % 2

    return prod;


def list_of_bits_to_integer(bits):
    bits.reverse()
    return int("".join(str(x) for x in bits), 2)

if __name__ == '__main__':
    try:
        a_txt = input('a: ')
        a = int(eval(a_txt))
        b_txt = input('b: ')
        b = int(eval(b_txt))

        print(f"'+' of {a_txt} and {b_txt}: {format(sum(a, b), '08b')}")
        print(f"'*' of {a_txt} and {b_txt}: {format(list_of_bits_to_integer(multiply_poly(a, b)), '016b')}")
        print(f"Modulo: {format(list_of_bits_to_integer(multiply_poly(a, b)) % 0b110110001, '016b')}")
        printPoly(multiply_poly(a, b))
    except Exception as e:
        print('Incorrect input: ' + str(e))
        raise e
